#!/bin/sh
#weekly routine on Sunday pm /Monday am. This script should be run from /canto-space/
# JWRN
# /canto-space/ == /data/export/canto/canto

# Define log file
LOGFILE="/var/log/canto_weekly.log"

# Function to log messages with date stamp
# e.g. log "Canto script completed successfully"
# generates something like
# 2020-05-20 10:24:37: Canto script completed successfully

function log (){
    DATESTAMP="$(date +"%Y-%m-%d %H:%M:%S")"
    /bin/echo "${DATESTAMP}: ${1}" >> "${LOGFILE}"
}

# copy/update of ontologies from /data/export/curfiles/ontologies/trunk/ into 'canto-space/import_export'
## gm comment: I don't really know how rsync works so I didn't know which options to choose (so didn't try to!) and the syntax may not be correct, but the first path should be OK if Canto is installed on the current vm.
## vt comment: the second path should be to /canto-space/import_export/

function update_obo_file()
{
	FILE_NAME=${1}

# first, check that the FILE exists and svn update if so

	if [ -e "/data/export/curfiles/ontologies/trunk/${FILE_NAME}" ] ; then

		log "Updating ${FILE_NAME} ..."
		/usr/bin/rsync /data/export/curfiles/ontologies/trunk/${FILE_NAME} ./import_export/${FILE_NAME}

	else

		log "'${FILE_NAME}' does not exist as a FILE, skipping ..."

	fi

}

for FILE in "fly_anatomy.obo" "flybase_controlled_vocabulary.obo" "fly_development.obo" "go-basic.obo"; do
	update_obo_file ${FILE}
done



#replace merged ontology and reload all ontologies to Canto
##vt comment: Ideally, add the following 'if' routine . If hard to implement, remove the 'if' routine and make the three commands run by default
## JWRN comment: how do we know an ontology has been changed? Is there a piece of information we can write out and read back in?
## JWRN comment: may be easier to just update whatever and improve in time

if <any ontology has been changed>, then

# redo/replace merged FBbt-GO.obo ontology
		sh ./FBbt-GO_routine/FBbt-GO_routine.sh
# replace extension_config.tsv
		sh ./extension_config-Rscript/list-to-extension_config.sh
# reload the ontologies	and extension configuration
		sudo ./canto/script/canto_docker ./script/canto_load.pl --process-extension-config --ontology  /import_export/FBbt-GO_test2.obo --ontology    /import_export/fly_development.obo --ontology   /import_export/flybase_controlled_vocabulary.obo
	
fi

# JWRN additions

# Function to retry command until sucessful with max number of attempts
function retry {

    # Starting number
    local n=1

    # Max number of attempts
    local max=5

    # Sleep period in seconds until retry
    local delay=2

    # Loop 
    while true; do

		# Run array of commands passed in and break loop if successful
		"${@}" && break || {

			# Else loop while attempt no is less than max
			if [[ "${n}" -lt "${max}" ]]; then

				# Increment attempt counter
				((n++))

				# log status
				log "Command failed. Attempt ${n}/${max}"

				# Sleep for period
				sleep ${delay}
			else
				# Hit max attempts and still failed so giving up
				log "${@} failed after ${n} attempts."
			fi
		}
    done
}

# Set filename of file to pull from 
MARKERFILE="./canto_done"

# Use retry function to pull marker file from deneb
# JWRN comment: requires ssh keys be setup for root to fbadmin
retry /usr/bin/scp fbadmin@deneb.pdb.cam.ac.uk:instance/canto_done "${MARKERFILE}"

# Get DBNAME from downloaded file
if [[ -e "${MARKERFILE}" ]]; then
    DBNAME=$(cat "${MARKERFILE}")
else
	log "${MARKERFILE} does not exist, cannot continue"
	exit 1
fi

# Check DBNAME is not blank
if [[ -z ${BDNAME }]]; then
    log "${DBNAME} is blank, cannot continue"
    exit 1
fi

# Transform $DBNAME 
CANTO_CONFIG="./canto_deploy.yaml"

if [[ -e "${CANTO_CONFIG}" ]]; then
	sed -i.bak -E "s/(^[[:space:]]+\-[[:space:]]\"dbi\:Pg\:dbname=)[[:alpha:]]+(\;[[:space:]]host=deneb\.pdn\.cam\.ac\.uk\")/\1${DBNAME}\2/" "${CANTO_CONFIG}"
else
	log "${CANTO_CONFIG} does not exist, cannot continue"
	exit 1
fi


#data import (using Gillian's scripts in the vm - see point 7.d in https://docs.google.com/document/d/19C-J8sJmZb_OSluxyzBWJxUkdR_N4sIpgjHI7u5pp0I/edit)
### gm comment: the following 'if' command should work
### run the script to generate new information into canto ONLY if the fbrf_input_list.tsv file exists

# make fbrf_input_list.tsv (list of newly thin-curated papers)
# if there are no new papers to add in a particular week, the output file,
# fbrf_input_list.tsv, will be empty
/usr/bin/perl /data/export/support_scripts/get_fbrfs_to_add_to_canto.pl /data/export/support_scripts/modules_server.cfg > fbrf_input_list.tsv


if [ -e "./fbrf_input_list.tsv" ] ; then # test for not empty

# make the json input file
	/usr/bin/perl /data/export/support_scripts/canto_json_input_maker.pl /data/export/support_scripts/modules_server.cfg ./fbrf_input_list.tsv > ./import-fbrfs.json

# load the json file into canto
	sudo ./canto/script/canto_docker ./script/canto_add.pl --sessions-from-json ./import-fbrfs.json vmt25@cam.ac.uk 7227

### remove the fbrf_input_list.tsv file once done, so that the script doesn't try to add the same information again next time its run
	/bin/rm ./fbrf_input_list.tsv

fi


#reset cache (restart memcached)
/etc/init.d/memcached restart

#canto restart
/etc/init.d/canto restart






