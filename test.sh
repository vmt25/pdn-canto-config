#!/bin/sh

# Called if total failure happens
function fail {
    # Echo output to stderr
    echo "${1}" >&2

    # Exit with non-zero code
    exit 1
}

function retry {
    # Define local vars

    # Starting number
    local n=1

    # Max number of attempts
    local max=5

    # Sleep period in seconds until retry
    local delay=2

    # Loop 
    while true; do

    # Run array of commands passed in and break loop if successful
    "${@}" && break || {

        # Else loop while attempt no is less than max
        if [[ "${n}" -lt "${max}" ]]; then

            # Increment attempt counter
            ((n++))

            # echo status
            echo "Command failed. Attempt ${n}/${max}"

            # Sleep for period
            sleep ${delay}
        else
            # Hit max attempts and still failed so giving up
            fail "The command has failed after ${n} attempts."
        fi
    }
    done
}

# Grab file from Daneb
FILENAME="marker_file.txt"

retry scp jwrn3@ent.csi.cam.ac.uk:"${FILENAME}" /tmp/

# Get DBNAME from downloaded file
if [[ -e "/tmp/${FILENAME}" ]]; then
    DBNAME=$(cat "/tmp/${FILENAME}")
fi

# Check DBNAME is not blank
if [[ -z ${BDNAME }]]; then
    echo "${DBNAME} is blank, cannot continue"
    exit 1
fi

# Transform $DBNAME 
sed -E "s/(^[[:space:]]+\-[[:space:]]\"dbi\:Pg\:dbname=)[[:alpha:]]+(\;[[:space:]]host=deneb\.pdn\.cam\.ac\.uk\")/\1${DBNAME}\2/" canto_deployment.yaml