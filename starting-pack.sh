#!/bin/sh

#before start, add the content of the folder 'starting-pack' to ./canto-space/
#run this @ /canto-space/

#add canto_deploy.yaml (the configuration file) to ./canto-space/canto/
##comment for vitor - add username and password for chado @ deneb?
mv canto_deploy.yaml ./canto/


#enable server mode

##comment for James: make sure the CANTO-SPACE variable of canto-docker-initd has the correct filepath
##create a canto-docker-initd file @ /sbin/ and replace the one @ ./canto/etc/
##make them executable
cp canto-docker-initd /sbin/
chmod a+x /sbin/canto-docker-initd
cp canto-docker-initd ./canto/etc/
chmod a+x ./canto/etc/canto-docker-initd
rm canto-docker-initd

##create a 'canto' file @ /etc/init.d
##and make it executable
mv canto_for_etc-initd /etc/init.d/canto
chmod a+x /etc/init.d/canto


#enable memcached - for caching of the servers 
##comment for vitor: not sure it needs more stuff - ask Kim??
sudo apt-get install memcached


#this loop creates cv terms for all the range of values for the 'priority curation score' within the internal canto database: 0-36
for i in {0..36}; do 
score="sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm \"Canto curation priorities\" "$i" FB:cantoscore"$i;
$score
done


#this section will create CV terms for triage statuses, so that the publication list can be sub-divifed into more specific lists. This list is the current in use for phenotype curation (e.g. disease, pheno, training, etc).
sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "DISEASE"
sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "PHENO"
sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "PHENO_ANAT"
sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "PHENO_CHEM"
sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "PHENO_DATASET"
sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "LOW_PRIORITY"
sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "TRAINING"
sudo ./canto/script/canto_docker ./script/canto_add.pl --cvterm "Canto publication triage status" "HIGH_PRIORITY"

#canfigure the taxon (Drosophila melanogaster)
sudo ./canto/script/canto_docker ./script/canto_add.pl --organism "Drosophila melanogaster" 7227 [fruit fly]


#start canto
/etc/init.d/canto start


#run the weekly routine script which executes all configuration steps
sh weekly_routine.sh


